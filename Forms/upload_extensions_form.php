<?php

class upload_extension_form extends moodleform
{
    public function definition()
    {
	$form = $this->_form;
	$form->addElement('filepicker', 'pluginfile', 'Add payments extensions', null, array('maxbytes' => 20480, 'accepted_types' => '*'));
	$form->setType('pluginfile', PARAM_RAW);
	$this->add_action_buttons();
	//echo '<pre>',print_r(get_class_methods($this));
    }
}
