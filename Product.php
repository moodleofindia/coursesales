<?php

interface Product
{
    public function getPrice();
    public function getDescription();
    public function getName(); 
    public function getPicture();
}

abstract class AbstractProduct implements Product
{
    protected $id;
    protected $name;
    protected $price;
    protected $description;
    protected $picture;
   
    public function renderer()
    {
       echo '|---------------------------|', "\n";
       echo '| Name : '.$this->name,"| \n";
       echo '| Description : '.$this->description,"| \n";
       echo '| Price : '.$this->price, "|\n";
       echo '|---------------------------|', "\n";
    }
   
    abstract public function pageRender();

    public function getPrice(){ return $this->price; }
    public function getName(){ return $this->name; }
    public function getDescription(){ return $this->description; }
    public function getPicture(){ return $this->picture; }

    public function __construct($name, $description, $price, $picture = '')
    {
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->picture = $picture;
    }
}

class CategoryProduct extends AbstractProduct
{
    public function pageRender()
    {
	echo __CLASS__;
	echo $this->name."\n";
	echo print_r(array('C programming', 'Java', 'PHP'));
    }
}

class CourseProduct  extends AbstractProduct
{
    public function pageRender()
    {
	echo __CLASS__;
	echo $this->name,"\n";
	echo print_r(array('C programming', 'Java', 'PHP'));
    }
}

class ScormProduct extends AbstractProduct
{
    public function pageRender()
    {
	echo __CLASS__;
	echo $this->name,"\n";
	echo print_r(array('C programming', 'Java', 'PHP'));
    }
}

function load($class, $name, $desc, $price)
{
    $cat = new $class($name, $desc, $price);
    $cat->renderer();
    $cat->pageRender();
}

//load($argv[hp1], $argv[2], $argv[3], $argv[4]);
