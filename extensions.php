<?php

//define('CLI_SCRIPT', true);
include '../config.php';
include $CFG->libdir.'/formslib.php';
include './Forms/upload_extensions_form.php';
// Geneate page
$PAGE->set_pagelayout('admin');
$PAGE->set_context(null);
$PAGE->set_url($CFG->wwwroot.'/productdev/extensions.php');
echo $OUTPUT->header();

$form = new upload_extension_form();

if($data = $form->get_data())
{
   $filename = $form->get_new_filename('pluginfile');
   $success = $form->save_file('pluginfile', $CFG->dirroot.'/productdev/PaymentGateway', false);
}

$form->display();

$table = new html_table();
$table->head = array('Extension type', 'Name', 'Visible', 'Settings');
$table->data = [
		['Payments Gatways', 'Paypal', '<i class="icon icon-hide"></i>', '<i class="icon icon-gear"></i>'],
	       ];
echo html_writer::table($table);
echo $OUTPUT->footer();
